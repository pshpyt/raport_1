class ChangePriceType < ActiveRecord::Migration
  def change
  	change_column :reports, :price, :integer
  end
end
