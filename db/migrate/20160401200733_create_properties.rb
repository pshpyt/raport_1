class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.integer :property_type
      t.integer :rooms
      t.string :address
      t.integer :surface
      t.decimal :price, :decimal, precision: 8, scale: 2

      t.timestamps null: false
    end
  end
end
