Rails.application.routes.draw do
  
  root 'home#index'
  get '/:locale', to: 'home#index'

  scope '/(:locale)', locale: /en|fr|pt/ do    
    get 'a2c', to: "reports#index"
    resources :reports, path: '/a2c/reports'

    # for A2C service related api 
    get 'a2c/:action',controller: :a2c
  end

end
