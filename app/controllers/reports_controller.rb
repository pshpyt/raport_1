class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  def index
    @reports = has_search_params? ? filter_search : Report.all
    @reports = @reports.page(params[:page]) if @reports.any?
  end

  def show
  end

  def new
    @report = Report.new
  end

  def edit
  end

  def create
    @report = Report.new(report_params)
    respond_to do |format|
      if @report.save
        format.html { redirect_to @report, notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render nothing: true, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_report
    @report = Report.find(params[:id])
  end

  def report_params
    params.require(:report).permit(:report_type,
                                     :rooms,
                                     :address,
                                     :surface,
                                     :price,
                                     :city,
                                     :description,
                                     :name,
                                     :phone,
                                     :email,
                                     :lat,
                                     :lng)
  end

  def filter_search
    name        = params[:contact_name]
    to_date     = parse_date(params[:to_date])
    from_date   = parse_date(params[:from_date])
    valid_dates = valid_dates?(from_date, to_date)

    if name.present? && valid_dates
      Report
        .where("name ILIKE ?", "%#{name}%")
        .where(created_at: range(from_date, to_date))
    elsif name.blank? && valid_dates
      Report
        .where(created_at: range(from_date, to_date))
    elsif name.present?
      Report
        .where("name ILIKE ?", "%#{name}%")
    else
      Report.all
    end
  end

  def range(from, to)
    (from.beginning_of_day)..(to.end_of_day)
  end

  def parse_date(date)
    DateTime.parse(date)
  rescue
    nil
  end

  def valid_dates?(from, to)
    from.present? && to.present? && from <= to
  end

  def has_search_params?
    params[:contact_name].present? || (params[:to_date].present? && params[:from_date].present?)
  end
end
