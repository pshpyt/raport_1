class Report < ActiveRecord::Base
  paginates_per 20
  default_scope { order('created_at DESC') }
  
  enum property_type: {house: 0,  flat: 1}
end
