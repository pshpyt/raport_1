// Google Autocomplete

function initAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById('address_field')),
    {types: ['geocode']}
  );
  autocomplete.addListener('place_changed', fillFields);
}

function getAngularScope(){
  return angular.element(document.getElementById('address_field')).scope();
}

function fillFields() {
  var place = autocomplete.getPlace();
  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();

  if(lat && lng){
    $scope = getAngularScope();
    if($scope){ // For Angular Case, edit page
      // Fill angular variables
      $scope.$apply(function(){
        $scope.property.address = document.getElementById('address_field').value;
        $scope.property.lat = lat;
        $scope.property.lng = lng;
      })
    } else { // For pure HTML case, new page
      document.getElementById('latitude_field').value = lat
      document.getElementById('longitude_field').value = lng
    }
  }
}


window.onload = function(){
  initAutocomplete();
}
document.addEventListener('DOMContentLoaded', function() {
  initAutocomplete();
}, false);

