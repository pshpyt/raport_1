// Line and Bar Data
var data = {
  animation: false,
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      fillColor: "rgba(220,220,220,0.2)",
      strokeColor: "rgba(220,220,220,1)",
      pointColor: "rgba(220,220,220,1)",
      pointStrokeColor: "#fff",
      pointHighlightFill: "#fff",
      pointHighlightStroke: "rgba(220,220,220,1)",
      data: [65, 59, 80, 81, 56, 55, 40]
    }
  ]
};

// Pie and Doughnut data
var pie_data = [
    {
        value: 300,
        color:"#F7464A",
        highlight: "#FF5A5E",
        label: "Red"
    },
    {
        value: 50,
        color: "#46BFBD",
        highlight: "#5AD3D1",
        label: "Green"
    },
    {
        value: 100,
        color: "#FDB45C",
        highlight: "#FFC870",
        label: "Yellow"
    }
] 

// Line
var line_options = 
  {
    tooltipTemplate: "<%= value %>",
    showTooltips: true,
    onAnimationComplete: function()
    {    
      this.showTooltip(this.datasets[0].points, true);          
    },
    tooltipEvents: [],
    animation: false
  }

// Bar
var bar_options = 
  {
    tooltipTemplate: "<%= value %>",
    showTooltips: true,
    onAnimationComplete: function()
    {    
      this.showTooltip(this.datasets[0].bars, true);          
    },
    tooltipEvents: [],
    animation: false
  }

// Pie and Doug options
var pie_options = 
  {
    tooltipTemplate: "<%= value %>",
    showTooltips: true,
    onAnimationComplete: function()
    {    
      this.showTooltip(this.segments, true);
    },
    tooltipEvents: [],
    animation: false
  }


// Load after DOM loaded
  
var init = function() {
  // Line Chart
  var ctxLineChart = document.getElementById("line-chart").getContext("2d");
  var myLineChart = new Chart(ctxLineChart).Line(data, line_options);

  // Bar Chart
  var ctxBarChart = document.getElementById("bar-chart").getContext("2d");
  var myBarChart = new Chart(ctxBarChart).Bar(data, bar_options);
  
  // Pie Chart
  var ctxPieChart = document.getElementById("pie-chart").getContext("2d");
  var myPieChart = new Chart(ctxPieChart).Pie(pie_data, pie_options);

  // Doughnut Chart
  var ctxDoughChart = document.getElementById("dough-chart").getContext("2d");
  var myDoughChart = new Chart(ctxDoughChart).Doughnut(pie_data, pie_options);
}

window.onload = init
